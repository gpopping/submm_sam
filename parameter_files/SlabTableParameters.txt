#With this parameter file you can initiate the creation of a new lookup table for slabs that are populatefd by molecular cloud. The output will be the sub-mm line luminosity per slab
#output file
tables/PowerlawAllZ_slab_fineRedshiftGrid.dat
#slope of molecular mass distribution function
-1.8
#SFR surface density that corresponds to 1 Habing Unit: UV_MW
0.001
#H2SurfaceDensityMin, H2SurfaceDensityMax, H2SurfaceDensityBinNumber, all in log Msun/pc^2
-1,5.0,40
#dR: size of the slab
101.
