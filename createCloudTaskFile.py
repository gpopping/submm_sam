#!/usr/bin/env python
"""Demonstrate the task-pull paradigm for high-throughput computing
using mpi4py. Task pull is an efficient way to perform a large number of
independent tasks when there are more tasks than processors, especially
when the run times vary for each task. 
This code is over-commented for instructional purposes.
This example was contributed by Craig Finch (cfinch@ieee.org).
Inspired by http://math.acadiau.ca/ACMMaC/Rmpi/index.html
"""

import numpy as np
#from mpi4py import MPI
import time
import itertools
import sys, os
import os.path



##########################################
#                                        #
#       Read INPUT parameters            #
#                                        #
##########################################
try:
    ParameterFile =  sys.argv[1]
    with open(ParameterFile) as fp:
        cnt = 0
        for line in fp:
            if not line.startswith('#'):
                cnt += 1
                print cnt, line
                if cnt == 1:
                    outdir = line
                    outdir = outdir.rstrip("\n")
                    tempdir = os.path.join(outdir, "temp/")
                elif cnt == 2:
                    fileName = line.rstrip("\n")
                    outname = os.path.join(outdir, fileName)
                elif cnt ==3:
                    NZONES = eval(line)
                elif cnt == 4:
                    ProfileType = line.rstrip("\n")
                elif cnt == 5:
                    McloudMin, McloudMax, numberMcloudBins = eval(line)
                elif cnt == 6:
                    PressureMin, PressureMax, numberPressureBins =  eval(line)
                elif cnt == 7:                    
                    MetalMin, MetalMax, numberMetalBins = eval(line)
                elif cnt == 8:
                    RadfieldMin, RadfieldMax, numberRadfieldBins = eval(line)
                elif cnt == 9:
                    RedshiftMin, RedshiftMax, numberRedshiftBins  = eval(line)
                elif cnt == 10:
                    clumping = eval(line)
                    if clumping == 1:
                        noClump = False
                    else:
                        noClump = True
            
    #print "Done Reading Parameter File"
except:
    print "No Input Parameterfile provided"

###########################################
#                                        #
#       Setting up grids                 #
#                                        #
##########################################

Mcloud = 10**np.linspace(McloudMin,McloudMax,numberMcloudBins)
P = 10**np.linspace(PressureMin, PressureMax, numberPressureBins)
Z = 10**np.linspace(MetalMin, MetalMax, numberMetalBins)
RadField = 10**np.linspace(RadfieldMin, RadfieldMax,numberRadfieldBins)
redshift = np.linspace(RedshiftMin, RedshiftMax, numberRedshiftBins)    




######################################
#                                    #
#   Create outpuf files              #
#                                    #
######################################
os.system('mkdir ' + tempdir)
######################################
#                                    #
#  Set up the grid for calculations  #
#                                    #
######################################
inp = np.array(list(itertools.product(Mcloud,P,Z,RadField,redshift)),dtype = np.float64)
INPUT = np.zeros((len(inp),6))
INPUT[:,:5] = inp
INPUT[:,5] = np.arange(len(INPUT))
del inp

print "Total number of points in the grid:",len(INPUT)
print ""


######################################
#                                    #
#  Start the running                 #
#                                    #
######################################

os.system('rm TaskFile')
TaskFile = open('./TaskFile', 'aw')
CloudsDone = []
for i in range(len(INPUT)):
    if os.path.isfile(os.path.join(tempdir, str(i) + '.0.dat')) == False:
        TaskFile.write('python scripts/despotic_cloud.py ' + str(INPUT[i,0]) + ' ' + str(INPUT[i,1]) + ' '  + str(INPUT[i,2]) + ' '  + str(INPUT[i,3]) + ' '  + str(INPUT[i,4]) + ' '  + str(INPUT[i,5]) + ' ' + str(NZONES) + ' ' + ProfileType  + ' ' + str(noClump) + ' ' + tempdir + '\n')
    else:
        #cloud already calculated, no need to include it
        CloudsDone = np.append(CloudsDone, i)

TaskFile.close()
print "Number of clouds already done in previous run:", len(CloudsDone)
print "Number of clouds remaining to do:", len(INPUT) - len(CloudsDone)
print ""


if len(CloudsDone) != len(INPUT):
    print "Created a TaskFile, please start running the code in batch mode"
else:
    os.system('rm TaskFile')
    print "Done with all the clouds for this input, create output file"
    os.system('rm '+ outname)
    outfile = open(outname,'aw')
    outfile.write('# 0 Mcloud (Msun)\n')
    outfile.write('# 1 Pressure (K cm^3) \n')
    outfile.write('# 2 Metalicity\n')
    outfile.write('# 3 Radiation Field (G0)\n')
    outfile.write('# 4 redshift\n')
    outfile.write('# 5 CII luminosity (erg/s)\n')
    outfile.write('# 6 CO 1-0 luminosity (erg/s)\n')
    outfile.write('# 7 CO 2-1 luminosity (erg/s)\n')
    outfile.write('# 8 CO 3-2 luminosity (erg/s)\n')
    outfile.write('# 9 CO 4-3 luminosity (erg/s)\n')
    outfile.write('# 10 CO 5-4 luminosity (erg/s)\n')
    outfile.write('# 11 [CI]1-0 luminosity (erg/s)\n')
    outfile.write('# 12 [CI]2-1 luminosity (erg/s)\n')
    outfile.write('# 13 fH2\n')
    outfile.write('# 14 CO abundance mass weighted \n')
    outfile.write('# 15 CO abundance luminosity weighted\n')
    outfile.write('# 16 CII abundance mass weighted\n')
    outfile.write('# 17 CII abundance luminosity weighted\n')
    outfile.write('# 18 C abundance mass weighted\n')
    outfile.write('# 19 C abundance luminosity weighted\n')
    outfile.write('# 20 Tgas mass weighted (K)\n')
    outfile.write('# 21 Tgas luminosity weighted (K)\n')
    outfile.write('# 22 Tdust mass weighted (K)\n')
    outfile.write('# 23 Tdust luminosity weighted (K)\n')
    outfile.write('# 24 Luminosity sub-mm (erg/s)\n')
    outfile.write('# 25 CO 6-5 luminosity (erg/s)\n')
    outfile.write('# 26 CO 7-6 luminosity (erg/s)\n')
    outfile.write('# 27 CO 8-7 luminosity (erg/s)\n')
    outfile.write('# 28 CO 9-8 luminosity (erg/s)\n')
    outfile.write('# 29 Luminosity [OI] 63 micon (erg/s)\n')
    outfile.write('# 30 Luminosity [OI] 44 micron (erg/s)\n')
    outfile.write('# 31 Luminosity [OI] 145 micronm (erg/s)\n')


    List = os.listdir(tempdir)
    for File in List:
        data = np.genfromtxt(tempdir + File)
        np.savetxt(outfile,[data], fmt = '%1.5e')
    outfile.close()
    os.system('rm -r ' + tempdir)
    
