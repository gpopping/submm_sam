'''
This code will calculate the sub-mm emission line luminosity of a single molecular cloud. It is built on Despotic.

'''
#Hand import DESPOTIC
import sys

# Import the despotic library and various standard python libraries
from despotic import cloud,zonedcloud
from despotic.chemistry import NL99_GC
import numpy as np
from astropy import units as u
from astropy import constants as constants
from astropy.cosmology import WMAP9 as cosmo
from scipy.integrate import quad


#np.seterr(divide = 'ignore',invalid='ignore')
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py

########################################################################
# User-settable options
########################################################################

#set number of radial zones in each cloud
DMR = 1.# Dust-to-medal ratio

#Clumping due to turbulence?
########################################################################
# Light-weighting as a function of the sub-mm continuum luminosity. Not very relevant
########################################################################
beta = -1.5
nu0 = 3.e12

wavmin = 8. * u.micron
wavmax = 1000. * u.micron
numin, numax = (constants.c / wavmin).to(u.Hz), (constants.c / wavmax).to(u.Hz)

#Next two functions define the sub-mm continuum SED
def tau(nu):
    return ((nu/nu0)**beta)
    
def BB(nu,T):
    A = 1 - np.exp(-tau(nu))
    B = 2. * constants.h.cgs / constants.c.cgs**2 * (nu/u.s)**3 / (np.exp(constants.h.cgs * nu/u.s / (constants.k_B.cgs * T)) -1)
    return A*B

#The continuum flux at some frequency and redshift used for the light-weighted lumninosityies.
def flux(nu, T, Mdust, redshift = 0.1):
    T = T * u.K
    Mdust = Mdust * u.Msun
    Bv = (BB(nu,T)/u.s / u.Hz).to('Jy')
    #print nu * Bv * Mdust * (1 + redshift) / (cosmo.luminosity_distance(redshift).to(u.pc))**2

    wav = (constants.c / (nu /u.s)).to(u.micron)
    kappa0 = 0.77 * u.cm**2/u.g
    kappa = kappa0 * (wav/(850. * u.micron))**-2
    Snu  = (kappa * Bv * Mdust *  np.pi * 4)  #the IR luminosity in erg/s/Hz
    return Snu.value

    
########################################################################
# Different density profiles for the molecular clouds
########################################################################
def densityProfile(mass,size,NZONES = 25, ProfileType = 'Plummer'):
    Radii = np.linspace(0*u.pc,size,NZONES+1)
    radii = (Radii[1:]+ Radii[:-1])/2
    dR = np.abs(Radii[1:] - Radii[:-1])
    
    #Plummer
    if ProfileType == 'Plummer':
        Rp = size/10
        density = (3 * mass / (4. * np.pi * Rp**3) * (1. + radii**2/Rp**2)**(-5./2))
        density = ((density /constants.m_p).cgs)[::-1]
    elif ProfileType == 'Powerlaw':
        #Powerlaws
        alpha = 2
        n0 = mass / (4. * np.pi *size**2 * size**(3. - alpha)/(3. - alpha))
        rho = n0 * (size/radii)**alpha
        density = ((rho /constants.m_p).cgs)[::-1]
    elif ProfileType == 'Logotropic':
        #Logotropic
        density = (mass / (4./3 * np.pi * size**3) / constants.m_p).cgs
        nHext = 2./3 * density
        density = nHext * (size / radii[::-1])

    return density, dR.to(u.cm)
    
########################################################################
# Code to calculate the sub-mm emission lumninosity of a single cloud
########################################################################
def submm_luminosity(INPUT, NZONES=25, ProfileType = 'Plummer',noClump = False):
    #Input[5] is irrelevant
    Mcloud,Pressure,Metallicity,RadField,redshift = INPUT[0], INPUT[1], INPUT[2], INPUT[3], INPUT[4]
    Mcloud *= u.Msun
    #================================================================
    #SUBGRID MODEL STUFF FOR CLOUDS
    #================================================================
    #usually we import these via some *.desp file.  Here, we
    #explicitly say these values as they depend on the metallicity
    #as well, and can impact how the CRs and UV radiation can get
    #in.

    #The radius of the cloud, as a function of the external pressure
    Rcloud =  (Pressure/1.e4)**(-0.25) * (Mcloud.value/290)**0.5 * u.pc
        
    #================================================================
    #The cloud column densities are defined. This is either a cloud with fixed volume density or some distribution
    if ProfileType != 'Fixed':
        density, dR = densityProfile(Mcloud, Rcloud, NZONES, ProfileType = ProfileType)
        density /=2.33 #atomic weight correction
        column_density_cgs = np.cumsum(density * dR)

        #set up the zoned cloud (a radially stratified cloud)
        gmc = zonedcloud(colDen = column_density_cgs.value)
        #Hard coded by hand to fix some weird zondecloud issues. Because zonedcloud is not so good with non-fixed cloud profiles
        gmc._colDen[1:] = column_density_cgs.value
        gmc.colDen = gmc._colDen[1:] - (gmc._colDen[1:] - gmc._colDen[:-1])/2
        gmc._colDen[1:] = column_density_cgs.value #For some reason gmc._colDen gets changed in the previous line... Have to reset
    else:
        #a fixed average density
        density = (Mcloud / (4./3 * np.pi * Rcloud**3) / constants.m_p).cgs
        density /= 2.33 #attomic weight correction
        column_density_cgs = density * Rcloud.to(u.cm)
        gmc = zonedcloud(colDen = np.linspace(column_density_cgs.value/NZONES,column_density_cgs.value,NZONES))
        gmc._colDen = np.linspace(column_density_cgs.value/NZONES,column_density_cgs.value,NZONES+1)
        gmc.colDen = gmc._colDen[1:] - (gmc._colDen[1:] - gmc._colDen[:-1])/2

        
    #================================================================   
    #assign other properties of clouds. Temperatures will be solved for later
    gmc.nH = density.value
    gmc.Td = 10.
    gmc.Tg = 10.
    gmc.rad.TradDust = 10.
    gmc.ionRate = 1.e-17*RadField
    gmc.rad.ionRate = 1.e-17*RadField
    gmc.chi = RadField 
    gmc.rad.chi = RadField

    
    #================================================================
    
        
    gmc.sigmaD10   = 2.0e-26  * Metallicity * DMR       # Cross section to 10K thermal radiation, cm^2 H^-1
    gmc.sigmaDPE   = 1.0e-21 * Metallicity * DMR       # Cross section to 8-13.6 eV photons, cm^2 H^-1
    gmc.sigmaDISRF = 3.0e-22  * Metallicity  * DMR      # Cross section to ISRF photons, cm^2 H^-1
    gmc.Zd      = 1.0  * Metallicity * DMR        # Dust abundance relative to solar
    gmc.alphaGD	   = 3.2e-34 * Metallicity * DMR	      # Dust-gas coupling coefficient, erg cm^3 K^-3/2

    gmc.dust.sigmaD10   = 2.0e-26  * Metallicity * DMR       # Cross section to 10K thermal radiation, cm^2 H^-1
    gmc.dust.sigmaDPE   = 1.0e-21 * Metallicity * DMR       # Cross section to 8-13.6 eV photons, cm^2 H^-1
    gmc.dust.sigmaDISRF = 3.0e-22  * Metallicity  * DMR      # Cross section to ISRF photons, cm^2 H^-1
    gmc.dust.Zd      = 1.0  * Metallicity * DMR        # Dust abundance relative to solar
    gmc.dust.alphaGD	   = 3.2e-34 * Metallicity * DMR	      # Dust-gas coupling coefficient, erg cm^3 K^-3/2

    #Initial abundance of elements
    gmc.addEmitter('c+',1.e-100*Metallicity)
    gmc.addEmitter('c',2.e-4*Metallicity)
    gmc.addEmitter('o', 4.e-4*Metallicity)
    gmc.addEmitter('co',1.e-100*Metallicity) 

    #CMB temperature as a function of redshift
    gmc.TCMB = 2.73 * (1. + redshift)
    gmc.rad.TCMB = 2.73 * (1. + redshift)



    #================================================================
    
    #initialise the abundances for H2 and He, and tell the code to
    #extrapolate from the collision tables if you hit a derived
    #temperature outside of the Leiden MolData
    for nz in range(NZONES):
        gmc.comp[nz].xH2 = 0.5
        gmc.comp[nz].xHe = 0.1
        gmc.emitters[nz]['co'].extrap = True
        gmc.emitters[nz]['c+'].extrap = True
        gmc.emitters[nz]['o'].extrap = True
        gmc.emitters[nz]['c'].extrap = True


    #================================================================
    
    #the first arrays that we're making space for are going to hold the
    #abundances of different species.  note that the abundances vary with
    #radial position in the clouds.
    gmc_xH_zones = np.zeros(NZONES)
    gmc_xH2_zones = np.zeros(NZONES)
    gmc_xCO_zones = np.zeros(NZONES)
    gmc_xCp_zones = np.zeros(NZONES)
    gmc_xC_zones = np.zeros(NZONES)
    gmc_xO_zones = np.zeros(NZONES)
    gmc_xOHx_zones = np.zeros(NZONES)
    gmc_radial_column_densities = np.zeros(NZONES)
    cumulative_gmc_masses = np.zeros(NZONES)
    gmc_radius = np.zeros(NZONES)
    H2_rates = []

     
    #================================================================

    #actually run the chemical equilibrium model.  this evolves
    #the temperature calcualtion in iteration with the chemistry
    #which is slower, but the most right thing to do as it
    #simultaneously solves for radiative transfer, chemistry and
    #temperature all at once. This is the hard of the codes and takes a while...
    gmc.setVirial() #set the cloud to virial properties
    gmc.setChemEq(network=NL99_GC, evolveTemp = 'iterateDust', verbose=True, info = {'xC': 2.e-4*Metallicity,'xO':4.e-4*Metallicity}, tol = 1.e-3)



    #save the abundances for each zone
    for nz in range(NZONES):
        gmc_xH_zones[nz] = gmc.chemabundances_zone[nz]['H']
        gmc_xH2_zones[nz] = gmc.chemabundances_zone[nz]['H2']
        gmc_xCO_zones[nz] = gmc.chemabundances_zone[nz]['CO']
        gmc_xCp_zones[nz] = gmc.chemabundances_zone[nz]['C+']
        gmc_xC_zones[nz] = gmc.chemabundances_zone[nz]['C']
        gmc_xO_zones[nz] = gmc.chemabundances_zone[nz]['O']
        gmc_xOHx_zones[nz] = gmc.chemabundances_zone[nz]['OHx']
                        
        gmc_radial_column_densities[nz] = gmc.colDen[nz]
        cumulative_gmc_masses[nz] =  np.sum(gmc.mass()[0:nz+1])/np.sum(gmc.mass())
        gmc_radius[nz] = gmc.radius()[nz]

    #THe H2 fraction of the cloud
    fH2 = np.sum(gmc.mass() * (2. * gmc_xH2_zones))/np.sum(gmc.mass())

    #Number of H2 nuclei
    H2_n_h_nuclei =  np.sum(gmc.mass()*u.g / (constants.m_p * gmc.muH ).to('g')) #Note that mass in zonedcloud is already only the hydrogen atoms

    #CII
    H2_rates.append(gmc.dEdt(fixedLevPop=False,noClump = noClump)) #get the cooling rates: note, I've already calculated the level populations before using LineLum, so take off the fixedlevpop if you haven't done that
    H2_LambdaCp = np.array([r['LambdaLine']['c+'] for r in H2_rates]) #just pull out the line cooling rate for C+
    H2_lcii = (H2_LambdaCp * H2_n_h_nuclei).decompose()*u.erg/u.s #LambdaCp was the cooling rate per h nucleon.   so i multiply by H2_n_h_nuclei which is my number of H per cloud, then by the number of the clouds in the galaxy

    if np.isnan(H2_lcii): H2_lcii = 0.0 *u.erg/u.s

    #CO
    gmclines = gmc.lineLum('co', noClump = noClump)
    CO10 =  (gmclines[0]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CO21 =  (gmclines[1]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CO32 =  (gmclines[2]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CO43 =  (gmclines[3]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CO54 =  (gmclines[4]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CO65 =  (gmclines[5]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CO76 =  (gmclines[6]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CO87 =  (gmclines[7]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CO98 =  (gmclines[8]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    if CO10.value <0: CO10*=0
    if CO21.value <0: CO21*=0
    if CO32.value <0: CO32*=0
    if CO43.value <0: CO43*=0
    if CO54.value <0: CO54*=0
    if CO65.value <0: CO65*=0
    if CO76.value <0: CO76*=0
    if CO87.value <0: CO87*=0
    if CO98.value <0: CO98*=0


    gmclines = gmc.lineLum('c', noClump = noClump)
    CI10 =  (gmclines[0]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    CI21 =  (gmclines[1]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s

    gmclines = gmc.lineLum('o', noClump = noClump)
    OI1 =  (gmclines[0]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    OI2 =  (gmclines[1]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
    OI3 =  (gmclines[2]['lumPerH'] * H2_n_h_nuclei).decompose()*u.erg/u.s
        
    #save the mass and luminosity weighted temperatures and abundances; 
    Ts = np.array([T for T in gmc.Tg])
    Ls = np.zeros(NZONES)

    #Calculate the total FIR luminosity for the luminosity weighting
    for i in range(NZONES):
        Ls[i] = ((quad(flux,numax.value, numin.value,args = (gmc.Tg[i],gmc.dust.Zd * 0.01 * ((gmc.mass()[i]*u.g).to(u.Msun)).value))[0] * u.erg/u.s).to(u.Lsun)).value

    #_mass means mass weighted
    #_lum means luminosity weighted
    gmc_Tg_mass = np.sum(gmc.Tg * gmc.mass())/np.sum(gmc.mass())
    gmc_Tg_lum =  np.sum(gmc.Tg * Ls) / np.sum(Ls)
    gmc_Td_mass = np.sum(gmc.Td * gmc.mass())/np.sum(gmc.mass())
    gmc_Td_lum =  np.sum(gmc.Td * Ls) / np.sum(Ls)

    
    #Abundances
    gmc_xCO_mass = np.average(gmc_xCO_zones, weights = gmc.mass())
    gmc_xCp_mass = np.average(gmc_xCp_zones, weights = gmc.mass())
    gmc_xC_mass = np.average(gmc_xC_zones, weights = gmc.mass())

    gmc_xCO_lum = np.average(gmc_xCO_zones, weights = Ls)
    gmc_xCp_lum = np.average(gmc_xCp_zones, weights = Ls)
    gmc_xC_lum = np.average(gmc_xC_zones, weights = Ls)

    return np.array([Mcloud.value, Pressure, Metallicity, RadField, redshift,H2_lcii.value[0],CO10.value, CO21.value, CO32.value, CO43.value, CO54.value, CI10.value, CI21.value,fH2,gmc_xCO_mass, gmc_xCO_lum,gmc_xCp_mass, gmc_xCp_lum, gmc_xC_mass, gmc_xC_lum, gmc_Tg_mass, gmc_Tg_lum, gmc_Td_mass, gmc_Td_lum, np.sum(Ls), CO65.value, CO76.value, CO87.value, CO98.value,OI1.value, OI2.value, OI3.value])


########################################################################
# Program code
########################################################################
#print submm_luminosity([1.e4, 1.e3, 1.0,1.0, 0.0])
args = sys.argv[1:]
INPUT = args[:5]
INPUT = [eval(i) for i in INPUT]
cloudNumber = args[5]
if args[8] == 'False':
    noClump = False
else: 
    noClump = True
tempdir = args[9]
NZONES = eval(args[6])
ProfileType = args[7]
try:
    result = submm_luminosity(INPUT, NZONES = NZONES, ProfileType = ProfileType, noClump = noClump)
except:
    result = np.array(np.append(INPUT,np.zeros(27)))


np.savetxt(tempdir  + str(cloudNumber) + '.dat',[result], fmt = '%1.5e') #Make sure no double instance is created  
