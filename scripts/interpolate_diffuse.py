#! /usr/bin/env python

import numpy as np
import scipy.interpolate

"""
This returns the luminosity in units of Lsun/H atom
"""

data =np.genfromtxt('tables/diffuseEmission_HI.dat')
Metal = data[:,0]
RadField = data[:,1]
CII = data[:,2]
CO10 = data[:,3]
CO21 = data[:,4]
CO32 = data[:,5]
CO43 = data[:,6]
CO54 = data[:,7]
CI10 = data[:,8]
CI21 = data[:,9]

interpolatorCII_HI = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CII)
interpolatorCO10_HI = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO10)
interpolatorCO21_HI = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO21)
interpolatorCO32_HI = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO32)
interpolatorCO43_HI = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO43)
interpolatorCO54_HI = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO54)
interpolatorCI10_HI = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CI10)
interpolatorCI21_HI = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CI21)



data =np.genfromtxt('tables/diffuseEmission_HII.dat')
Metal = data[:,0]
RadField = data[:,1]
CII = data[:,2]
CO10 = data[:,3]
CO21 = data[:,4]
CO32 = data[:,5]
CO43 = data[:,6]
CO54 = data[:,7]
CI10 = data[:,8]
CI21 = data[:,9]

interpolatorCII_HII = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CII)
interpolatorCO10_HII = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO10)
interpolatorCO21_HII = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO21)
interpolatorCO32_HII = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO32)
interpolatorCO43_HII = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO43)
interpolatorCO54_HII = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CO54)
interpolatorCI10_HII = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CI10)
interpolatorCI21_HII = scipy.interpolate.interp2d(np.log10(Metal),np.log10(RadField),CI21)


def interpolate(Metallicity, RadField, ISM_type):
    if ISM_type == 'HI':
        CII = interpolatorCII_HI(np.log10(Metallicity),np.log10(RadField))[0]
        CO10 = interpolatorCO10_HI(np.log10(Metallicity),np.log10(RadField))[0]
        CO21 = interpolatorCO21_HI(np.log10(Metallicity),np.log10(RadField))[0]
        CO32 = interpolatorCO32_HI(np.log10(Metallicity),np.log10(RadField))[0]
        CO43 = interpolatorCO43_HI(np.log10(Metallicity),np.log10(RadField))[0]
        CO54 = interpolatorCO54_HI(np.log10(Metallicity),np.log10(RadField))[0]
        CI10 = interpolatorCI10_HI(np.log10(Metallicity),np.log10(RadField))[0]
        CI21 = interpolatorCI21_HI(np.log10(Metallicity),np.log10(RadField))[0]
    elif ISM_type == 'HII':
        CII = interpolatorCII_HII(np.log10(Metallicity),np.log10(RadField))[0]
        CO10 = interpolatorCO10_HII(np.log10(Metallicity),np.log10(RadField))[0]
        CO21 = interpolatorCO21_HII(np.log10(Metallicity),np.log10(RadField))[0]
        CO32 = interpolatorCO32_HII(np.log10(Metallicity),np.log10(RadField))[0]
        CO43 = interpolatorCO43_HII(np.log10(Metallicity),np.log10(RadField))[0]
        CO54 = interpolatorCO54_HII(np.log10(Metallicity),np.log10(RadField))[0]
        CI10 = interpolatorCI10_HII(np.log10(Metallicity),np.log10(RadField))[0]
        CI21 = interpolatorCI21_HII(np.log10(Metallicity),np.log10(RadField))[0]
    return CII, CO10, CO21, CO32, CO43, CO54, CI10, CI21



#print interpolate(1.0,1.0,'HI')
