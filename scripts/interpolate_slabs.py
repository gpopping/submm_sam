#! /usr/bin/env python
"""
small m and l denote mass weighted and light weighted
"""


import numpy as np


#Read data
data       = np.genfromtxt("tables/PlummerAllZ_slabCMB.dat")
data[data < 0] = 0.0
data[np.isnan(data)] = 0.0
Sigma_H2   = data[:,0]
Pressure   = data[:,1]
Z          = data[:,2]
Redshift   = data[:,3]
CIIintensity  = data[:,4]
CO10intensity  = data[:,5]
CO21intensity  = data[:,6]
CO32intensity  = data[:,7]
CO43intensity  = data[:,8]
CO54intensity = data[:,9]
CI10intensity = data[:,10]
CI21intensity = data[:,11]
XCOm        = data[:,12]
XCOl        = data[:,13]
XCpm        = data[:,14]
XCpl        = data[:,15]
XCm        = data[:,16]
XCl        = data[:,17]
Tgm        = data[:,18]
Tgl        = data[:,19]
Tdm        = data[:,20]
Tdl        = data[:,21]
Lsubmm     = data[:,22]
SigHI      = data[:,23]
SigHI[np.isnan(SigHI)] = 0.0
SigHI[np.isinf(SigHI)] = 0.0

pressure = np.unique(Pressure)
redshift = np.unique(Redshift)
sigma_H2 = np.unique(Sigma_H2)
metallicity = np.unique(Z)
redshift = np.unique(Redshift)



def interpolate(Input):
    SigmaIN,PIN,ZIN, redshiftIN = Input[0],Input[1],Input[2], Input[3]
    ind = np.where(sigma_H2 > SigmaIN)[0]
    if len(ind) == len(sigma_H2):
        Sigmabin = 0
        return 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    elif  len(ind) ==0:
        Sigmabin = len(sigma_H2) - 2
        defSigma = sigma_H2[-1]
    else:
        Sigmabin = ind[0] - 1
        defSigma = SigmaIN

    ind = np.where(pressure > PIN)[0]
    if len(ind) == len(Pressure):
        Pressurebin = 0
        return 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    elif  len(ind) ==0:
        Pressurebin = len(pressure) - 2
        defPressure = pressure[-1]
    else:
        Pressurebin = ind[0] - 1
        defPressure = PIN
        
    ind = np.where(metallicity > ZIN)[0]
    if len(ind) == len(metallicity):
        Zbin = 0
        defZ = metallicity[0]
        return 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    elif  len(ind) ==0:
        Zbin = len(metallicity) - 2
        defZ = metallicity[-1]
    else:
        Zbin = ind[0] - 1
        defZ = ZIN

    """
    ind = np.where(radField > radFieldIN)[0]
    if len(ind) == len(radField):
        radFieldbin = 0
        defradField = radField[0]
        return 0,0,0,0,0,0,0,0,0,0
    elif  len(ind) ==0:
        radFieldbin = len(radField) - 2
        defradField = radField[-1]
    else:
        radFieldbin = ind[0] - 1
        defradField = radFieldIN
    """
    ind = np.where(redshift > redshiftIN)[0]
    if len(ind) == len(redshift):
        redshiftbin = 0
        defredshift = redshift[0]
    elif  len(ind) ==0:
        redshiftbin = len(redshift) - 2
        defredshift = redshift[-1]
    else:
        redshiftbin = ind[0] - 1
        defredshift = redshiftIN

    #
    CII_emission = 0.0
    CO10_emission = 0.0
    CO21_emission = 0.0
    CO32_emission = 0.0
    CO43_emission = 0.0
    CO54_emission = 0.0
    CI10_emission = 0.0
    CI21_emission = 0.0
    XCO_finm = 0.0
    XCO_finl = 0.0
    XC_finm  = 0.0
    XC_finl  = 0.0
    XCp_finm = 0.0
    XCp_finl = 0.0
    Tg_finm = 0.0
    Tg_finl = 0.0
    Td_finm = 0.0
    Td_finl = 0.0
    Lsubmm_fin = 0.0
    sigHI_fin = 0.0 #
    for h in np.array([Pressurebin,Pressurebin+1]):
        weightP = 1 - (np.abs(np.log10(defPressure) - np.log10(pressure[h]))) / (np.log10(pressure[Pressurebin+1]) -np.log10(pressure[Pressurebin]))
        ind = np.where((Pressure == pressure[h]))[0]
        
        for i in np.array([Sigmabin,Sigmabin+1]):
            weightSigma = 1 - (np.abs(np.log10(defSigma) - np.log10(sigma_H2[i]))) / (np.log10(sigma_H2[Sigmabin+1]) -np.log10(sigma_H2[Sigmabin]))
            ind2 = ind[np.where((Sigma_H2[ind] == sigma_H2[i]))[0]]
            for j in np.array([Zbin,Zbin+1]):

                weightZ = 1 - (np.abs(np.log10(defZ) - np.log10(metallicity[j]))) / (np.log10(metallicity[Zbin+1]) - np.log10(metallicity[Zbin]))
                ind3 = ind2[np.where((Z[ind2] == metallicity[j]))[0]]
                for l in np.array([redshiftbin,redshiftbin+1]):
                    weightredshift = 1 - (np.abs(defredshift - redshift[l])) / (redshift[redshiftbin+1] - redshift[redshiftbin])
                    ind4 = ind3[np.where((Redshift[ind3] == redshift[l]))[0]]
                    try:
                        CII_emission  += weightP * weightSigma * weightZ  *weightredshift *CIIintensity[ind4][0]
                    except:
                        print ind4
                        raise SystemExit
                    CO10_emission  += weightP * weightSigma * weightZ *weightredshift *CO10intensity[ind4][0]
                    CO21_emission  += weightP * weightSigma * weightZ *weightredshift *CO21intensity[ind4][0]
                    CO32_emission  += weightP * weightSigma * weightZ *weightredshift *CO32intensity[ind4][0]
                    CO43_emission  += weightP * weightSigma * weightZ *weightredshift *CO43intensity[ind4][0]
                    CO54_emission  += weightP * weightSigma * weightZ *weightredshift *CO54intensity[ind4][0]
                    CI10_emission  += weightP * weightSigma * weightZ *weightredshift *CI10intensity[ind4][0]
                    CI21_emission  += weightP * weightSigma * weightZ *weightredshift *CI21intensity[ind4][0]
                    XCO_finm  += weightP * weightSigma * weightZ *weightredshift *XCOm[ind4][0]
                    XCO_finl  += weightP * weightSigma * weightZ *weightredshift *XCOl[ind4][0]
                    XCp_finm  += weightP * weightSigma * weightZ *weightredshift *XCpm[ind4][0]
                    XCp_finl  += weightP * weightSigma * weightZ *weightredshift *XCpl[ind4][0]
                    XC_finm  += weightP * weightSigma * weightZ *weightredshift *XCm[ind4][0]
                    XC_finl  += weightP * weightSigma * weightZ *weightredshift *XCl[ind4][0]
                    Tg_finm  += weightP * weightSigma * weightZ *weightredshift *Tgm[ind4][0]
                    Tg_finl  += weightP * weightSigma * weightZ *weightredshift *Tgl[ind4][0]
                    Td_finm  += weightP * weightSigma * weightZ *weightredshift *Tdm[ind4][0]
                    Td_finl  += weightP * weightSigma * weightZ *weightredshift *Tdl[ind4][0]
                    Lsubmm_fin += weightP * weightSigma * weightZ *weightredshift *Lsubmm[ind4][0]
                    sigHI_fin += weightP * weightSigma * weightZ *weightredshift *SigHI[ind4][0]                        
             
    if CO10_emission < 0:
        print "Negative emission, something went wrong"
        raise SystemExit
    
    return CII_emission, CO10_emission, CO21_emission, CO32_emission, CO43_emission,CO54_emission, CI10_emission, CI21_emission,XCO_finm,XCO_finl, XCp_finm,XCp_finl, XC_finm,XC_finl, Tg_finm,Tg_finl, Td_finm, Td_finl,Lsubmm_fin, sigHI_fin






