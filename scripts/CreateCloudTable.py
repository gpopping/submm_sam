#!/usr/bin/env python
"""Demonstrate the task-pull paradigm for high-throughput computing
using mpi4py. Task pull is an efficient way to perform a large number of
independent tasks when there are more tasks than processors, especially
when the run times vary for each task. 
This code is over-commented for instructional purposes.
This example was contributed by Craig Finch (cfinch@ieee.org).
Inspired by http://math.acadiau.ca/ACMMaC/Rmpi/index.html
"""

import numpy as np
from mpi4py import MPI
import time
import despotic_cloud
import clouds_done
import itertools
import sys, os
import os.path



##########################################
#                                        #
#       Read INPUT parameters            #
#                                        #
##########################################
try:
    ParameterFile =  sys.argv[1]
    with open(ParameterFile) as fp:
        cnt = 0
        for line in fp:
            if not line.startswith('#'):
                cnt += 1
                print cnt, line
                if cnt == 1:
                    outdir = line
                    outdir = outdir.rstrip("\n")
                    tempdir = os.path.join(outdir, "temp/")
                elif cnt == 2:
                    fileName = line.rstrip("\n")
                    outname = os.path.join(outdir, fileName)
                elif cnt ==3:
                    NZONES = eval(line)
                elif cnt == 4:
                    ProfileType = line.rstrip("\n")
                elif cnt == 5:
                    McloudMin, McloudMax, numberMcloudBins = eval(line)
                elif cnt == 6:
                    PressureMin, PressureMax, numberPressureBins =  eval(line)
                elif cnt == 7:                    
                    MetalMin, MetalMax, numberMetalBins = eval(line)
                elif cnt == 8:
                    RadfieldMin, RadfieldMax, numberRadfieldBins = eval(line)
                elif cnt == 9:
                    RedshiftMin, RedshiftMax, numberRedshiftBins  = eval(line)
                elif cnt == 10:
                    clumping = eval(line)
                    if clumping == 1:
                        noClump = False
                    else:
                        noClump = True
            
    #print "Done Reading Parameter File"
except:
    print "No Input Parameterfile provided"

###########################################
#                                        #
#       Setting up grids                 #
#                                        #
##########################################

Mcloud = 10**np.linspace(McloudMin,McloudMax,numberMcloudBins)
P = 10**np.linspace(PressureMin, PressureMax, numberPressureBins)
Z = 10**np.linspace(MetalMin, MetalMax, numberMetalBins)
RadField = 10**np.linspace(RadfieldMin, RadfieldMax,numberRadfieldBins)
redshift = np.linspace(RedshiftMin, RedshiftMax, numberRedshiftBins)    



##########################################
#                                        #
#      Some modules to turn off          #
#   warnings and unnecessary prints      #
#                                        #
##########################################
import warnings
no_warnings = True
if no_warnings:
    warnings.filterwarnings("ignore")

# Disable
block_printing = True
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


###########################
#                         #
#   Create handy tags     #
#                         #
###########################
def enum(*sequential, **named):
    """Handy way to fake an enumerated type in Python
    http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python
    """
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

######################################
#                                    #
#   Set up parallel environments     #
#                                    #
######################################
# Define MPI message tags
tags = enum('READY', 'DONE', 'EXIT', 'START')

# Initializations and preliminaries
comm = MPI.COMM_WORLD   # get MPI communicator object
size = comm.size        # total number of processes
rank = comm.rank        # rank of this process
status = MPI.Status()   # get MPI status object

if rank == 0:
    time1 = time.time()


    ######################################
    #                                    #
    #   Create outpuf files              #
    #                                    #
    ######################################
    os.system('mkdir ' + tempdir)
    ######################################
    #                                    #
    #  Set up the grid for calculations  #
    #                                    #
    ######################################
    inp = np.array(list(itertools.product(Mcloud,P,Z,RadField,redshift)),dtype = np.float64)
    INPUT = np.zeros((len(inp),6))
    INPUT[:,:5] = inp
    INPUT[:,5] = np.arange(len(INPUT))
    del inp
    
    print "Total number of grids we'll be working on is",len(INPUT)
    print ""
    NumFilesDone = clouds_done.get_start_cloud(tempdir)
    INPUT = INPUT[NumFilesDone:]

    ######################################
    #                                    #
    #  Start the running                 #
    #                                    #
    ######################################
    # Master process executes code below        
    task_index = 0
    num_tasks = len(INPUT)
    num_workers = size - 1
    closed_workers = 0
    print("Master starting with %d workers" % num_workers)
    while closed_workers < num_workers:
        data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)

        source = status.Get_source()
        tag = status.Get_tag()
        if tag == tags.READY:
            # Worker is ready, so send it a task
            if task_index < num_tasks:
                print("Sending task %d of %d to worker %d" % (task_index+1, num_tasks, source))
                comm.isend(np.append([task_index+1],INPUT[task_index]), dest=source, tag=tags.START)
                task_index += 1                
            else:
                comm.isend(np.empty(4,dtype = np.float64), dest=source, tag=tags.EXIT)

        elif tag == tags.DONE:
            print("Got a result from worker %d" % source)

        elif tag == tags.EXIT:
            print("Worker %d is done and is exiting." % source)
            closed_workers += 1

    print("Master finishing took %s hours" % str((time.time() - time1)/3600))
    print("Saving master file")




    os.system('rm '+ outname)
    outfile = open(outname,'aw')
    outfile.write('# 0 Mcloud (Msun)\n')
    outfile.write('# 1 Pressure (K cm^3) \n')
    outfile.write('# 2 Metalicity\n')
    outfile.write('# 3 Radiation Field (G0)\n')
    outfile.write('# 4 redshift\n')
    outfile.write('# 5 CII luminosity (erg/s)\n')
    outfile.write('# 6 CO 1-0 luminosity (erg/s)\n')
    outfile.write('# 7 CO 2-1 luminosity (erg/s)\n')
    outfile.write('# 8 CO 3-2 luminosity (erg/s)\n')
    outfile.write('# 9 CO 4-3 luminosity (erg/s)\n')
    outfile.write('# 10 CO 5-4 luminosity (erg/s)\n')
    outfile.write('# 11 [CI]1-0 luminosity (erg/s)\n')
    outfile.write('# 12 [CI]2-1 luminosity (erg/s)\n')
    outfile.write('# 13 fH2\n')
    outfile.write('# 14 CO abundance mass weighted \n')
    outfile.write('# 15 CO abundance luminosity weighted\n')
    outfile.write('# 16 CII abundance mass weighted\n')
    outfile.write('# 17 CII abundance luminosity weighted\n')
    outfile.write('# 18 C abundance mass weighted\n')
    outfile.write('# 19 C abundance luminosity weighted\n')
    outfile.write('# 20 Tgas mass weighted (K)\n')
    outfile.write('# 21 Tgas luminosity weighted (K)\n')
    outfile.write('# 22 Tdust mass weighted (K)\n')
    outfile.write('# 23 Tdust luminosity weighted (K)\n')
    outfile.write('# 24 Luminosity sub-mm (erg/s)\n')
    outfile.write('# 25 CO 6-5 luminosity (erg/s)\n')
    outfile.write('# 26 CO 7-6 luminosity (erg/s)\n')
    outfile.write('# 27 CO 8-7 luminosity (erg/s)\n')
    outfile.write('# 28 CO 9-8 luminosity (erg/s)\n')
    outfile.write('# 29 Luminosity [OI] 63 micon (erg/s)\n')
    outfile.write('# 30 Luminosity [OI] 44 micron (erg/s)\n')
    outfile.write('# 31 Luminosity [OI] 145 micronm (erg/s)\n')


    List = os.listdir(tempdir)
    for File in List:
        data = np.genfromtxt(tempdir + File)
        np.savetxt(outfile,[data], fmt = '%1.5e')
    outfile.close()
    os.system('rm -r ' + tempdir)
    print "Worked on this for",  (time.time() - time1)/60, 'minutes'            

else:
    # Worker processes execute code below
    name = MPI.Get_processor_name()
    print("I am a worker with rank %d on %s." % (rank, name))
    while True:
        comm.isend(np.empty(9,dtype = np.float64), dest=0, tag=tags.READY)
        while not comm.Iprobe(source = 0):
            time.sleep(0.1)        
        INPUT = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
        tag = status.Get_tag()        
        if tag == tags.START:
            # Do the work here
            time2 = time.time()
            if os.path.isfile(tempdir + str(INPUT[-1]) + '.dat'):
                print "Already done. Worker",rank, 'took', time.time() - time2, "seconds"
                comm.isend(np.empty(9,dtype=np.float64), dest=0, tag=tags.DONE)            
            else:
                try:
                    if block_printing:
                        blockPrint()
                        result = despotic_cloud.submm_luminosity(INPUT[1:],NZONES=NZONES, ProfileType = ProfileType,noClump = noClump)
                        enablePrint()
                    else:
                        result = despotic_cloud.submm_luminosity(INPUT[1:],ProfileType = ProfileType)
                except:
                    result = np.zeros(32)
                    result[:5] = INPUT[1:6]
                print "Worker",rank, 'took', time.time() - time2, "seconds"
                np.savetxt(tempdir  + str(INPUT[-1]) + '.dat',[result], fmt = '%1.5e') #Make sure no double instance is created
                print "Done with task " + str(INPUT[0])
                comm.isend(np.empty(9,dtype=np.float64), dest=0, tag=tags.DONE)
        elif tag == tags.EXIT:
            break

    comm.isend(np.empty(9,dtype = np.float64), dest=0, tag=tags.EXIT)


