#! /usr/bin/env python

import numpy as np
import pandas as pd

#Read data
data = pd.read_table("tables/CloudGridPowerlawRedshifts.dat", sep='\s+',comment='#', header=None)
data.fillna(0, inplace=True)
data = data.as_matrix()

data[data < 0] = 0.0
Mcloud   = (data[:,0])
Pressure   = (data[:,1])
Z          = (data[:,2])
RadField   = (data[:,3])
Redshift   = data[:,4]
CIIintensity  = data[:,5]
CO10intensity  = data[:,6]
CO21intensity  = data[:,7]
CO32intensity  = data[:,8]
CO43intensity  = data[:,9]
CO54intensity  = data[:,10]
CI10intensity  = data[:,11]
CI21intensity  = data[:,12]
fh2            = data[:,13]
XCO_mass        = data[:,14]
XCO_lum        = data[:,15]
XCp_mass        = data[:,16]
XCp_lum        = data[:,17]
XC_mass        = data[:,18]
XC_lum        = data[:,19]
Tg_mass        = data[:,20]
Tg_lum        = data[:,21]
Td_mass        = data[:,22]
Td_lum        = data[:,23]
Lum        = data[:,24]

def parameter_ranges():
    return np.unique(Pressure), np.unique(Z), np.unique(RadField), np.unique(Redshift),np.unique(Mcloud)


radField = np.unique(RadField)
pressure = np.unique(Pressure)
redshift = np.unique(Redshift)
sigma_H2 = np.unique(Mcloud)
metallicity = np.unique(Z)
redshift = np.unique(Redshift)


def interpolate(Input):
    SigmaIN,PIN,ZIN,radFieldIN, redshiftIN = Input[0],Input[1],Input[2], Input[3],Input[4]
    ind = np.where(sigma_H2 > SigmaIN)[0]
    if len(ind) == len(sigma_H2):
        Sigmabin = 0
        return 0,0,0,0,0,0,0,0,0,0,0
    elif  len(ind) ==0:
        Sigmabin = len(sigma_H2) - 2
        defSigma = sigma_H2[-1]
    else:
        Sigmabin = ind[0] - 1
        defSigma = SigmaIN

    ind = np.where(pressure > PIN)[0]
    if len(ind) == len(Pressure):
        Pressurebin = 0
        return 0,0,0,0,0,0,0,0,0,0,0
    elif  len(ind) ==0:
        Pressurebin = len(pressure) - 2
        defPressure = pressure[-1]
    else:
        Pressurebin = ind[0] - 1
        defPressure = PIN
        
    ind = np.where(metallicity > ZIN)[0]
    if len(ind) == len(metallicity):
        Zbin = 0
        defZ = metallicity[0]
        return 0,0,0,0,0,0,0,0,0,0,0
    elif  len(ind) ==0:
        Zbin = len(metallicity) - 2
        defZ = metallicity[-1]
    else:
        Zbin = ind[0] - 1
        defZ = ZIN

    ind = np.where(radField > radFieldIN)[0]
    if len(ind) == len(radField):
        radFieldbin = 0
        defradField = radField[0]
        return 0,0,0,0,0,0,0,0,0,0,0
    elif  len(ind) ==0:
        radFieldbin = len(radField) - 2
        defradField = radField[-1]
    else:
        radFieldbin = ind[0] - 1
        defradField = radFieldIN

    ind = np.where(redshift > redshiftIN)[0]
    if len(ind) == len(redshift):
        redshiftbin = 0
        defredshift = redshift[0]
    elif  len(ind) ==0:
        redshiftbin = len(redshift) - 2
        defredshift = redshift[-1]
    else:
        redshiftbin = ind[0] - 1
        defredshift = redshiftIN

    #
    CII_emission = 0.0
    CO10_emission = 0.0
    CO21_emission = 0.0
    CO32_emission = 0.0
    CO43_emission = 0.0
    CO54_emission = 0.0
    CI10_emission = 0.0
    CI21_emission = 0.0
    fH2_fin = 0.0
    XCO_mass_fin = 0.0
    XCO_lum_fin = 0.0
    XC_mass_fin  = 0.0
    XC_lum_fin  = 0.0
    XCp_mass_fin = 0.0
    XCp_lum_fin = 0.0
    Tg_mass_fin = 0.0
    Tg_lum_fin = 0.0
    Td_mass_fin = 0.0
    Td_lum_fin = 0.0
    L_fin = 0.0
    #
 
    for h in np.array([Pressurebin,Pressurebin+1]):
        weightP = 1 - (np.abs(np.log10(defPressure) - np.log10(pressure[h]))) / (np.log10(pressure[Pressurebin+1]) -np.log10(pressure[Pressurebin]))
        ind = np.where((Pressure == pressure[h]))[0]
        
        for i in np.array([Sigmabin,Sigmabin+1]):
            weightSigma = 1 - (np.abs(np.log10(defSigma) - np.log10(sigma_H2[i]))) / (np.log10(sigma_H2[Sigmabin+1]) -np.log10(sigma_H2[Sigmabin]))
            ind2 = ind[np.where((Mcloud[ind] == sigma_H2[i]))[0]]
            for j in np.array([Zbin,Zbin+1]):
                weightZ = 1 - (np.abs(np.log10(defZ) - np.log10(metallicity[j]))) / (np.log10(metallicity[Zbin+1]) - np.log10(metallicity[Zbin]))
                ind3 = ind2[np.where((Z[ind2] == metallicity[j]))[0]]
                for k in np.array([radFieldbin,radFieldbin+1]):
                    weightradField = 1 - (np.abs(np.log10(defradField) - np.log10(radField[k]))) / np.abs(np.log10(radField[radFieldbin+1]) - np.log10(radField[radFieldbin]))
                    ind4 = ind3[np.where((RadField[ind3] == radField[k]))[0]]
                    for l in np.array([redshiftbin,redshiftbin+1]):
                        weightredshift = 1 - (np.abs(defredshift - redshift[l])) / (redshift[redshiftbin+1] - redshift[redshiftbin])
                        ind5 = ind4[np.where((Redshift[ind4] == redshift[l]))[0]]
                        try:
                            CII_emission  += weightP * weightSigma * weightZ * weightradField * weightredshift *CIIintensity[ind5][0]
                            CO10_emission  += weightP * weightSigma * weightZ * weightradField * weightredshift *CO10intensity[ind5][0]
                            CO21_emission  += weightP * weightSigma * weightZ * weightradField * weightredshift *CO21intensity[ind5][0]
                            CO32_emission  += weightP * weightSigma * weightZ * weightradField * weightredshift *CO32intensity[ind5][0]
                            CO43_emission  += weightP * weightSigma * weightZ * weightradField * weightredshift *CO43intensity[ind5][0]
                            CO54_emission  += weightP * weightSigma * weightZ * weightradField * weightredshift *CO54intensity[ind5][0]
                            CI10_emission  += weightP * weightSigma * weightZ * weightradField * weightredshift *CI10intensity[ind5][0]
                            CI21_emission  += weightP * weightSigma * weightZ * weightradField * weightredshift *CI21intensity[ind5][0]
                            fH2_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *fh2[ind5][0]
                            XCO_mass_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *XCO_mass[ind5][0]
                            XCO_lum_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *XCO_lum[ind5][0]
                            XCp_mass_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *XCp_mass[ind5][0]
                            XCp_lum_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *XCp_lum[ind5][0]
                            XC_mass_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *XC_mass[ind5][0]
                            XC_lum_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *XC_lum[ind5][0]
                            Tg_mass_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *Tg_mass[ind5][0]
                            Tg_lum_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *Tg_lum[ind5][0]
                            Td_mass_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *Td_mass[ind5][0]
                            Td_lum_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift *Td_lum[ind5][0]
                            L_fin  += weightP * weightSigma * weightZ * weightradField * weightredshift * Lum[ind5][0]
                        except:
                            pass
    if CO10_emission < 0:
        return 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.

    return CII_emission, CO10_emission, CO21_emission, CO32_emission, CO43_emission,CO54_emission, CI10_emission, CI21_emission, fH2_fin, XCO_mass_fin,XCO_lum_fin, XCp_mass_fin, XCp_lum_fin, XC_mass_fin, XC_lum_fin, Tg_mass_fin, Tg_lum_fin, Td_mass_fin, Td_lum_fin, L_fin


#index = 0
INPUT = [1.e+04, 1.00000e+05, 1.0,1.0, 0.0]
#Sigma is actually cloud mass
print interpolate(INPUT)

