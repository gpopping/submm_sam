import os
import numpy as np


def missing_elements(L):
    start, end = L[0], L[-1]
    return sorted(set(xrange(start, end + 1)).difference(L))

def get_start_cloud(tempdir):
    files = os.listdir(tempdir)
    files = sorted(np.array([eval(string.replace(".0.dat","")) for string in files]))
    if len(files) != 0:
        try:
            return missing_elements(files)[0]
        except:
            return files[-1]
    else: return 0


