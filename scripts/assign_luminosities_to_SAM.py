#! /usr/bin/env python
"""
This script calculates the line-luminosity of SAM galaxies.
"""
import numpy as np
import matplotlib.pyplot as pl
from multiprocessing import Pool
import multiprocessing
import interpolate_slabs
import interpolate_diffuse
import scipy.optimize as so
import pandas as pd
import os
import astropy.units as u
import astropy.constants as constants
from tqdm import tqdm

print "No warnings will be given"
import warnings
warnings.filterwarnings("ignore")

#Number of processors available 
PoolNumber = multiprocessing.cpu_count()

#Some galaxy properties
MassUnit = 1.e9 #Solar masses
f_Helium = 1.36
sigma_HII = 0.3
chi_gas = 1.7 #Ratio between gas and stellar discs
FVEL0 = 0.25 #ratio between gas and stellar vertical velocity dispersion.
dR = 500. #pc; resolution at which exponential distribution is build


def UVrad_field(sigma_gas,fH2):
    return 0.6 * sig_SFR(sigma_gas, fH2) / 0.001 #UV radiation field in Habing Units

def sig_SFR(sigma_gas,fH2):
    A_bigiel = 1.36*4.4E-03
    N_SB = 1.0
    SIGMA_SB_CRIT = 70.0 
    sfr_r = (A_bigiel/(10.0))*sigma_gas*fH2* (1.0+(sigma_gas*fH2)/SIGMA_SB_CRIT)**N_SB  #SFR surface dnesity in Msol/kpc^2
    return sfr_r


def fH2_GK(sig_gas,SFR,Z):
    """
    Metallicity and UV radiation field recipe for H2 fractions for Gneding and Kravtsov 2011
    """        
    DMW = Z
    UMW = SFR #assuming radiation field scales as SFR/SFRMW with SFRMW == 1.0 Msun/yr

    #fitting function from GK 2011
    Dstar = 1.5E-03*np.log(1.0+(3.0*UMW)**1.7)
    s = 0.04/(Dstar+DMW)
    alpha = 5.0*(0.5*UMW/(1.0+np.sqrt(0.5*UMW)))
    g_gk = (1.0+alpha*s+np.sqrt(s))/(1.0+s);

    Lambda_GK = np.log(1.0+g_gk*DMW**0.428571*(UMW/15.0)**0.571429)
    Sigma_star = 20.0*Lambda_GK**0.571429 /(DMW*np.sqrt(1.0+UMW*np.sqrt(DMW)))
        
    #Molecular fraction of gas surface densities
    fH2 = (1.0+Sigma_star/sig_gas)**-2.0
    return fH2

def get_luminosity(INPUT):
    Mgas,Mstar,MHI,MHII,Re,Z,SFR,Redshift = INPUT[0], INPUT[1],INPUT[2],INPUT[3],INPUT[4], INPUT[5], INPUT[6],INPUT[7]
    r = np.arange(0,10.*Re,dR)

    #Gas surface densities
    sigma0 = Mgas / (2. * np.pi * Re**2)
    sigma_gas = sigma0 * np.exp(-r/Re)/ f_Helium  - sigma_HII#correction for Helium and HII
    sigma_gas[sigma_gas<0] = 0.0

    #Star surface densities
    sigma0star = Mstar / (2. * np.pi * (Re/1.7)**2)
    sigma_star = sigma0star * np.exp(-r/(1.7 * Re))

    #fH2 fractions
    fH2 = fH2_GK(sigma_gas,SFR,Z)

    #SFR surface density
    sig_sfr = sig_SFR(sigma_gas,fH2)
    
    #Radiation_Fields
    RadFieldInput = SFR #For the diffuse gas

    #Pressure
    Pressure = 4.624E-16*sigma_gas*(sigma_gas +FVEL0*np.sqrt(sigma_star*sigma0star))
    Pressure = Pressure/7.262194266406942E-18 #to go to P/kb (i.e. K cm^-3)

    #Correction for the pressure
    alpha0, beta0 = 0.4,0.25
    Pressure /= (1. + alpha0 + beta0) 

    #Create output properties
    CII_lum, CO10_lum, CO21_lum, CO32_lum, CO43_lum,CO54_lum, CI10_lum, CI21_lum, Lsubmm= 0,0,0,0,0,0,0,0,0
    xCOm, xCpm, xCm, Tgm, Tdm, xCOl, xCpl, xCl, Tgl, Tdl,Sig_gas,Ls, Sig_HI = np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)),np.zeros(len(r)), np.zeros(len(r))

    
    
    if len(r) > 0:
        for i in range(len(r)):
            #Calculate emission per annulus
            if (sigma_gas[i] * fH2[i] > 0.1):
                #We only do this when the H2 surface density is > 1. The total emission hardly changes for lower surface densities. This could be easily changed by running the lookup tables again
                CII_slab,CO10_slab, CO21_slab, CO32_slab, CO43_slab, CO54_slab, CI10_slab, CI21_slab, xCOtm, xCOtl,xCptm, xCptl, xCtm,xCtl, Tgtm,Tgtl, Tdtm,Tdtl,Lsubmm_slab, sighi = interpolate_slabs.interpolate([sigma_gas[i]*fH2[i],Pressure[i],Z,Redshift])
            else:
                CII_slab,CO10_slab, CO21_slab, CO32_slab, CO43_slab, CO54_slab, CI10_slab, CI21_slab, xCOtm, xCOtl,xCptm, xCptl, xCtm,xCtl, Tgtm,Tgtl, Tdtm,Tdtl,Lsubmm_slab, sighi = 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0
            if np.isnan(CII_slab):
                 CII_slab,CO10_slab, CO21_slab, CO32_slab, CO43_slab, CO54_slab, CI10_slab, CI21_slab, xCOtm, xCOtl,xCptm, xCptl, xCtm,xCtl, Tgtm,Tgtl, Tdtm,Tdtl,Lsubmm_slab, sighi = 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0

            
                 
            #Bookkeeping of total luminosities
            area = 2. * np.pi * r[i] * dR #in pc^2
            #pl.plot(sigma_gas[i],area*CII_slab,'.b')
           
            CII_lum +=  area  * CII_slab 
            CO10_lum += area * CO10_slab
            CO21_lum += area * CO21_slab
            CO32_lum += area * CO32_slab
            CO43_lum += area * CO43_slab
            CO54_lum += area * CO54_slab
            CI10_lum += area * CI10_slab
            CI21_lum += area * CI21_slab
            Lsubmm += area * Lsubmm_slab
            xCOm[i], xCpm[i], xCm[i],Tgm[i], Tdm[i]  = xCOtm, xCptm, xCtm, Tgtm, Tdtm
            xCOl[i], xCpl[i], xCl[i],Tgl[i], Tdl[i]  = xCOtl, xCptl, xCtl, Tgtl, Tdtl
            Sig_HI[i] = sighi * 2. * np.pi * r[i] * dR
            Sig_gas[i] =  sigma_gas[i]*fH2[i] * 2. * np.pi * r[i] * dR
            Ls[i] = Lsubmm_slab * area

    #Correct HI mass for HI locked up in clouds. Important for diffuse gas calculations
    #The molecular clouds from DESPOTIC are not 100% H2, this has to be accounted for.
    MHI_cloud = np.sum(Sig_HI) #HI at the outside of molecular clouds


    MHI -= MHI_cloud
    if MHI <0:
        MHI_cloud = np.abs(MHI_cloud)
        MHI = 0.0
        MHII -= MHI_cloud  #Store it in HII
        
    CII_dif = 0.0 


    #Atomic hydrogen
    #There is always additional background radiation
    if RadFieldInput <= 1.0: RadFieldInput = 1.0        
    CII_dif, CO10_dif, CO21_dif, CO32_dif, CO43_dif, CO54_dif, CI10_dif, CI21_dif = interpolate_diffuse.interpolate(Z,RadFieldInput, 'HI')
    numHatoms_dif = ((MHI) * u.Msun/constants.m_p).cgs / 2.33 #numHatoms
    CII_dif *= numHatoms_dif
    CII_HI = CII_dif
    CII_lum += CII_dif
    CO10_lum += numHatoms_dif * CO10_dif
    CO21_lum += numHatoms_dif * CO21_dif
    CO32_lum += numHatoms_dif * CO32_dif
    CO43_lum += numHatoms_dif * CO43_dif
    CO54_lum += numHatoms_dif * CO54_dif
    CI10_lum += numHatoms_dif * CI10_dif
    CI21_lum += numHatoms_dif * CI21_dif

    """
    #Ionized Hydrogen
    CII_dif, CO10_dif, CO21_dif, CO32_dif, CO43_dif, CO54_dif, CI10_dif, CI21_dif  = interpolate_diffuse.interpolate(Z,RadFieldInput, 'HII')
    numHatoms_dif = ((MHII) * u.Msun/constants.m_p).cgs / 2.33 #numHatoms

    CII_dif *= numHatoms_dif
    CII_lum += CII_dif
    CII_dif += CII_HI
    CO10_lum += numHatoms_dif * CO10_dif
    CO21_lum += numHatoms_dif * CO21_dif
    CO32_lum += numHatoms_dif * CO32_dif
    CO43_lum += numHatoms_dif * CO43_dif
    CO54_lum += numHatoms_dif * CO54_dif
    CI10_lum += numHatoms_dif * CI10_dif
    CI21_lum += numHatoms_dif * CI21_dif
    """
    #Working out global properties
    ind = np.where(xCpm > 0)[0]
    if CII_lum>0:
        xCpm = np.sum(xCpm[ind] * sig_sfr[ind]) / np.sum(sig_sfr[ind])
        xCOm = np.sum(xCOm[ind] * sig_sfr[ind]) / np.sum(sig_sfr[ind])
        xCm = np.sum(xCm[ind] * sig_sfr[ind]) / np.sum(sig_sfr[ind])
        Tgm = np.sum(Tgm[ind] * sig_sfr[ind]) / np.sum(sig_sfr[ind])
        Tdm = np.sum(Tdm[ind] * sig_sfr[ind]) / np.sum(sig_sfr[ind])
        xCpl = np.sum(xCpl[ind] * Ls[ind]) / np.sum(Ls[ind])
        xCOl = np.sum(xCOl[ind] * Ls[ind]) / np.sum(Ls[ind])
        xCl = np.sum(xCl[ind] * Ls[ind]) / np.sum(Ls[ind])
        Tgl = np.sum(Tgl[ind] * Ls[ind]) / np.sum(Ls[ind])
        Tdl = np.sum(Tdl[ind] * Ls[ind]) / np.sum(Ls[ind])

    else:
        xCOm = 1.e-99
        xCpm = 1.e-99
        xCm = 1.e-99
        Tgm = 1.e-99
        Tdm = 1.e-99
        xCOl = 1.e-99
        xCpl = 1.e-99
        xCl = 1.e-99
        Tgl = 1.e-99
        Tdl = 1.e-99
    del Sig_gas,Ls, Sig_HI
    Pressure = np.sum(Pressure * sig_sfr)/ np.sum(sig_sfr)
    
   
    return CII_lum.value, CO10_lum.value,CO21_lum.value,CO32_lum.value,CO43_lum.value,CO54_lum.value, CI10_lum.value, CI21_lum.value,xCOm, xCOl, xCpm, xCpl, xCm, xCl,Tgm, Tgl,Tdm,Tdl,Lsubmm,CII_dif.value, Pressure

    
def run_submm_line_calculator(InDirec, OutDirec, OutName, printLineOnly = True, ModelType = 'Nbody', EPSredshift = -99):
    if (ModelType == 'Nbody'):
        pass
    elif (ModelType == 'EPS'):
        if EPSredshift <0:
            print "Give a proper redshift for the EPS merger tree"
            raise SystemExit
    else:
        print "No valid ModelType was definied. Should be either 'Nbody' or 'EPS'"
        raise SystemExit


        
    outname = OutDirec + OutName
    print 'Set up output file...', outname
    os.system('rm '+ outname)
    outfile = open(outname,'aw')
    if printLineOnly == False:
        outfile.write('# 0 host halo id\n')
        outfile.write('# 1 birth halo id\n')
        outfile.write('# 2 redshift\n')    
        outfile.write('# 3 log [CII] luminosity Lsol\n')
        outfile.write('# 4 log CO 1-0 luminosity Lsol\n')
        outfile.write('# 5 log CO 2-1 luminosity Lsol\n')
        outfile.write('# 6 log CO 3-2 luminosity Lsol\n')
        outfile.write('# 7 log CO 4-3 luminosity Lsol\n')
        outfile.write('# 8 log CO 5-4 luminosity Lsol\n')
        outfile.write('# 9 log CI 1-0 luminosity Lsol\n')
        outfile.write('# 10 log CI 2-1 luminosity Lsol\n')
        outfile.write('# 11 log xCO CO abundance mass weighted\n')
        outfile.write('# 12 log xCO CO abundance light weighted\n')
        outfile.write('# 13 log xCp CII abundance mass weighted\n')
        outfile.write('# 14 log xCp CII abundance light weighted\n')
        outfile.write('# 15 log xC [C] abundance mass weighted\n')
        outfile.write('# 16 log xC [C] abundance light weighted\n')
        outfile.write('# 17 log Tg Kelvin mass weighted\n')
        outfile.write('# 18 log Tg Kelvin light weighted\n')
        outfile.write('# 19 log Td Kelvin mass weighted\n')
        outfile.write('# 20 log Td Kelvin light weighted\n')
        outfile.write('# 21 log sub_mm luminosity Lsol\n')
        outfile.write('# 22 log [CII] luminosity from diffuse regions Lsol\n')
        outfile.write('# 23 log Pressure/kb K cm^-3\n')
    else:
        outfile.write('# 0 host halo id\n')
        outfile.write('# 1 birth halo id\n')
        outfile.write('# 2 redshift\n')    
        outfile.write('# 3 log [CII] luminosity Lsol\n')
        outfile.write('# 4 log CO 1-0 luminosity Lsol\n')
        outfile.write('# 5 log CO 2-1 luminosity Lsol\n')
        outfile.write('# 6 log CO 3-2 luminosity Lsol\n')
        outfile.write('# 7 log CO 4-3 luminosity Lsol\n')
        outfile.write('# 8 log CO 5-4 luminosity Lsol\n')
        outfile.write('# 9 log CI 1-0 luminosity Lsol\n')
        outfile.write('# 10 log CI 2-1 luminosity Lsol\n')

    print "Start reading from", InDirec
    FullDataIterated = pd.read_csv(InDirec + 'galprop.dat', sep=' ',comment='#', header = None, iterator=True, chunksize = 10000)
    for iter_num, data in enumerate(tqdm(FullDataIterated)):
        data = data.as_matrix()

        if ModelType == 'EPS':
            halo_id = data[:,0]
            birthhalo_id = data[:,1]
            mstar = 10**data[:,10]
            mgas = 10**data[:,12]
            mHI = 10**data[:,14]
            mHII = 10**data[:,15]    
            r_disk = data[:,3] * 1.e3 #in pc
            Zcold = data[:,22]
            sfr = data[:,24]
            redshift = np.zeros(len(mgas)) + EPSredshift        
        elif ModelType == 'Nbody':
            halo_id = data[:,0]
            birthhalo_id = data[:,1]
            redshift = data[:,2]
            mstar = data[:,7]*MassUnit
            mgas = data[:,14]*MassUnit
            mHI = data[:,15]*MassUnit
            mHII = data[:,17]*MassUnit
            r_disk = data[:,12] * 1.e3 #in pc                                                                                                                       
            Zcold = data[:,19]/mgas*MassUnit
            sfr = data[:,20]
            #x,y,z = data[:,33], data[:,34], data[:,35]

        INPUT = np.zeros((len(redshift),8))
        INPUT[:,0] = mgas
        INPUT[:,1] = mstar
        INPUT[:,2] = mHI
        INPUT[:,3] = mHII
        INPUT[:,4] = r_disk
        INPUT[:,5] = Zcold
        INPUT[:,6] = sfr
        INPUT[:,7] = redshift

        pool = Pool(processes = PoolNumber)
        out = np.array(pool.map(get_luminosity,INPUT))
        pool.close()

        
        #table = np.column_stack((halo_id[broken_array[i]:broken_array[i+1]],birthhalo_id[broken_array[i]:broken_array[i+1]], redshift[broken_array[i]:broken_array[i+1]], np.log10(out), x[broken_array[i]:broken_array[i+1]],y[broken_array[i]:broken_array[i+1]],z[broken_array[i]:broken_array[i+1]]))
        if printLineOnly == False:
            table = np.column_stack((halo_id,birthhalo_id, redshift, np.log10(out)))
        else:
            table = np.column_stack((halo_id,birthhalo_id, redshift, np.log10(out[:,:8])))
        np.savetxt(outfile, table)#, fmt = '%1.3e') 
        del out
    outfile.close()





   

