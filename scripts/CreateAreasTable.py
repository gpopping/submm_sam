#! /usr/bin/env python
import numpy as np
import matplotlib.pyplot as pl
import numpy.random as nr
from multiprocessing import Pool
from scipy.integrate import quad
import scipy.optimize as so
from functools import partial
from copy import copy
import time as time
import astropy.units as u
import astropy.constants as constants
import warnings as warnings
import interpolate_clouds as submm_interp
import itertools
import os
import sys
warnings.simplefilter('ignore',UserWarning)
import multiprocessing
PoolNumber = multiprocessing.cpu_count()

#Seed for reproduction
nr.seed(1234)
time0 = time.time()


##########################################
#                                        #
#       Read INPUT parameters            #
#                                        #
##########################################
try:
    ParameterFile =  sys.argv[1]
    with open(ParameterFile) as fp:
        cnt = 0
        for line in fp:
            if not line.startswith('#'):
                cnt += 1
                if cnt == 1:
                    outname = line.rstrip("\n")
                elif cnt == 2:
                    slope = eval(line)
                elif cnt ==3:
                    UV_MW = eval(line)
                elif cnt == 4:
                    H2SurfaceDensityMin, H2SurfaceDensityMax, H2SurfaceDensityBinNumber = eval(line)
                elif cnt == 5:
                    dR = eval(line)
except:
    print "No Input Parameterfile provided"

Sigma_H2 = 10**np.linspace(H2SurfaceDensityMin,H2SurfaceDensityMax,H2SurfaceDensityBinNumber)
P, Z, RadField,Redshift,CloudMasses = submm_interp.parameter_ranges()
CloudMinMass,CloudMaxMass = np.log10(np.min(CloudMasses)), np.log10(np.max(CloudMasses))


if dR**2 * np.min(Sigma_H2) < 10**CloudMinMass:
    print "\n The slab with the lowest surface density has a total H2 mass less than the lowest mass cloud in your lookup table. This will cause problems"
    print "Either increase the size of the slab dR, or increase the minimum H2 surface density you are interested in"
    print ""
    raise SystemExit




def slice_sampler(px, N = 1, x = []):
    """
    Provides samples from a user-defined distributionp.
    slice_sampler(px, N = 1, x = None)
    Inputs:
    px = A discrete probability distributionp.
    N = Number of samples to return, default is 1
    x = Optional list/array of observation values to return, where prob(x) = px.
    
    Outputs:
    If x=None (default) or if len(x) != len(px), it will return an array of integers
    between 0 and len(px)-1. If x is supplied, it will return the
    samples from x according to the distribution px.
    """
    values = np.zeros(N, dtype=np.int)
    samples = np.arange(len(px))
    px = np.array(px) / (1.*sum(px))
    u = nr.uniform(0, max(px))
    for i in xrange(N):
        included = px>=u
        choice = nr.choice(range(np.sum(included)), 1)[0]
        values[i] = samples[included][choice]
        u = nr.uniform(0, px[included][choice])
    if x != []:
        if len(x) == len(px):
            x=np.array(x)
            values = x[values]
        else:
            print "px and x are different lengths. Returning index locations for px."
    if N == 1:
        return values[0]
    return values


def px(M):
    return M**slope


def UVrad_field(sigma_H2):
    A_bigiel = 1.36*4.4E-03
    N_SB = 1.0
    SIGMA_SB_CRIT = 70.0 
    sfr_r = (A_bigiel/(10.0))*sigma_H2* (1.0+(sigma_H2)/SIGMA_SB_CRIT)**N_SB  #SFR surface dnesity in Msol/kpc^2
    return 0.6 * sfr_r / UV_MW #UV radiation field in Habing Units


def get_submm_lums(INPUT):
    Sigma_H2,P,Z,Redshift = INPUT[0], INPUT[1], INPUT[2], INPUT[3]
    Mtot = dR**2 * Sigma_H2
    Mmin =CloudMinMass
    Mmax = np.log10(Mtot)
    if Mmax >CloudMaxMass:
        Mmax = CloudMaxMass    
    Mdis = []
    count = 0
    Mrange =slice_sampler(px(10**np.linspace(Mmin,Mmax,1000)),N = 1000,x = 10**np.linspace(Mmin,Mmax,1000)) 

    while (np.sum(Mdis) < Mtot) & (Mmax > Mmin):
        M =slice_sampler(px(10**np.linspace(Mmin,Mmax,1000)),N = 1,x = 10**np.linspace(Mmin,Mmax,1000)) 
        Mdis = np.append(Mdis,M)
        count+=1
        Mmax = np.log10(10**Mmax - M)

    Mdis = np.array(Mdis)
    P = np.zeros(count) + P
    Z = np.zeros(count) +Z
    Redshift = np.zeros(count) +Redshift
    UVRadField = UVrad_field(Sigma_H2)

    #Need this checker because RadField self-consistently calculated and not as a part of input
    if UVRadField < RadField.min():
        UVRadField = RadField.min()
    elif UVRadField > RadField.max():
        UVRadField = RadField.max()

    UVRadField = np.zeros(count) + UVRadField
    numClouds = len(Mdis)
    
    return Mdis, P, Z, UVRadField,Redshift, numClouds



def set_up_interpolation(INPUT):
    #Set up arrays for efficient interpolation
    #Empty arrays
    Mdistot = np.zeros(int(5e8))
    Ptot = np.zeros(int(5e8))
    Ztot = np.zeros(int(5e8))
    RadFieldtot = np.zeros(int(5e8))
    Redshifttot = np.zeros(int(5e8))
    counts = np.zeros(len(INPUT))
    
    counter = 0
    print "Get clouds properties..."
    for i,inp in enumerate(INPUT):
        mdis,pdis, zdis,radfielddis, redshiftdis,numClouds = get_submm_lums(inp)
        Mdistot[counter:counter + numClouds] = mdis
        Ptot[counter:counter + numClouds] = pdis
        Ztot[counter:counter + numClouds] = zdis
        RadFieldtot[counter:counter + numClouds] = radfielddis
        Redshifttot[counter:counter + numClouds] = redshiftdis
        counts[i] = numClouds
        counter += numClouds
    Mdistot= Mdistot[:counter]
    Ptot = Ptot[:counter]
    Ztot = Ztot[:counter]
    RadFieldtot = RadFieldtot[:counter]
    Redshifttot = Redshifttot[:counter]

    print 'Number of clouds to interpolate', len(Ptot)
    print 'Interpolating...'
    INPUT2 = np.array([Mdistot,Ptot,Ztot,RadFieldtot,Redshifttot]).T

    #Interpolating
    pool = Pool(processes = PoolNumber)
    out = np.array(map(submm_interp.interpolate, INPUT2))
    pool.close()
    del INPUT2

    CII_emission,CO10_emission,CO21_emission,CO32_emission,CO43_emission,CO54_emission, CI10_emission, CI21_emission,fH2_mass,xCp_mass, xCp_lum,xCO_mass,xCO_lum,xC_mass,xC_lum,Tg_mass,Tg_lum,Td_mass,Td_lum, submm_lum = out[:,0], out[:,1], out[:,2], out[:,3], out[:,4], out[:,5], out[:,6], out[:,7], out[:,8], out[:,9],out[:,10], out[:,11], out[:,12], out[:,13], out[:,14], out[:,15], out[:,16], out[:,17], out[:,18],out[:,19]
    CII_emission, CO10_emission, CO21_emission, CO32_emission, CO43_emission, CO54_emission, CI10_emission, CI21_emission = CII_emission/3.826e33/dR**2, CO10_emission/3.826e33/dR**2, CO21_emission/3.826e33/dR**2, CO32_emission/3.826e33/dR**2, CO43_emission/3.826e33/dR**2, CO54_emission/3.826e33/dR**2, CI10_emission/3.826e33/dR**2, CI21_emission/3.826e33/dR**2 #convert to solar luminosities

    print "Reconstructing individual clouds..."
    counter = 0
    CIIout, CO10out, CO21out, CO32out, CO43out, CO54out, CI10out, CI21out,fH2out, xCOout_mass, xCOout_lum, xCpout_mass, xCpout_lum, xCout_mass, xCout_lum, Tgout_mass, Tgout_lum, Tdout_mass, Tdout_lum, Lum_submm, SigHI = np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT)),np.zeros(len(INPUT)),np.zeros(len(INPUT)),np.zeros(len(INPUT)),np.zeros(len(INPUT)),np.zeros(len(INPUT)),np.zeros(len(INPUT)),np.zeros(len(INPUT)),np.zeros(len(INPUT)),np.zeros(len(INPUT)), np.zeros(len(INPUT)), np.zeros(len(INPUT))


    counts = np.array([int(i) for i in counts])
    for i,numclouds in enumerate(counts):
        if np.sum(submm_lum[counter:counter+numclouds]) == 0.0:
            submm_lum[counter:counter+numclouds]+=1        
        if np.sum(Mdistot[counter:counter+numclouds]) == 0.0:
            Mdistot[counter:counter+numclouds]+=1        

        #Not all gas in DESPOTIC clouds is molecular, therefore work out the molecular fractions of the total cloud ensemble and multiply emission with 1./fH2. This is a correction
        fH2out[i] = np.sum(fH2_mass[counter:counter+numclouds] * Mdistot[counter:counter+numclouds])/np.sum(Mdistot[counter:counter+numclouds])
        CIIout[i] = np.sum(CII_emission[counter:counter+numclouds]) * 1./fH2out[i]
        CO10out[i] = np.sum(CO10_emission[counter:counter+numclouds]) * 1./fH2out[i]
        CO21out[i] = np.sum(CO21_emission[counter:counter+numclouds]) * 1./fH2out[i]
        CO32out[i] = np.sum(CO32_emission[counter:counter+numclouds]) * 1./fH2out[i]
        CO43out[i] = np.sum(CO43_emission[counter:counter+numclouds]) * 1./fH2out[i]
        CO54out[i] = np.sum(CO54_emission[counter:counter+numclouds]) * 1./fH2out[i]
        CI10out[i] = np.sum(CI10_emission[counter:counter+numclouds]) * 1./fH2out[i]
        CI21out[i] = np.sum(CI21_emission[counter:counter+numclouds]) * 1./fH2out[i]
        SigHI[i] = np.sum(Mdistot[counter:counter+numclouds]) * (1 - fH2out[i]) /fH2out[i]/dR**2  #The total HI surface density in all these clouds
        try:
            xCOout_mass[i] = np.average(xCO_mass[counter:counter+numclouds],weights = Mdistot[counter:counter+numclouds])
            xCOout_lum[i] = np.average(xCO_lum[counter:counter+numclouds],weights = submm_lum[counter:counter+numclouds])
            xCpout_mass[i] = np.average(xCp_mass[counter:counter+numclouds],weights = Mdistot[counter:counter+numclouds])
            xCpout_lum[i] = np.average(xCp_lum[counter:counter+numclouds],weights = submm_lum[counter:counter+numclouds])
            xCout_mass[i] = np.average(xC_mass[counter:counter+numclouds],weights = Mdistot[counter:counter+numclouds])
            xCout_lum[i] = np.average(xC_lum[counter:counter+numclouds],weights = submm_lum[counter:counter+numclouds])
            Tgout_mass[i] = np.average(Tg_mass[counter:counter+numclouds],weights = Mdistot[counter:counter+numclouds])
            Tgout_lum[i] = np.average(Tg_lum[counter:counter+numclouds],weights = submm_lum[counter:counter+numclouds])
            Tdout_mass[i] = np.average(Td_mass[counter:counter+numclouds],weights = Mdistot[counter:counter+numclouds])
            Tdout_lum[i] = np.average(Td_lum[counter:counter+numclouds],weights = submm_lum[counter:counter+numclouds])
            Lum_submm[i] = np.sum(submm_lum[counter:counter+numclouds])
        except:
            pass
        counter += numclouds


    return [CIIout, CO10out, CO21out, CO32out, CO43out, CO54out, CI10out, CI21out, xCOout_mass, xCOout_lum, xCpout_mass,xCpout_lum, xCout_mass,xCout_lum, Tgout_mass, Tgout_lum, Tdout_mass, Tdout_lum, Lum_submm, SigHI]



print 'Write output file...'
os.system('rm '+ outname)
outfile = open(outname,'aw')
outfile.write('# 0 Mcloud (Msun)\n')
outfile.write('# 1 Pressure (K cm^-3)\n')
outfile.write('# 2 Metallicity\n')
outfile.write('# 3 Redshift\n')
outfile.write('# 4 CII luminosity Lsol/pc^2\n')
outfile.write('# 5 CO 1-0 luminosity Lsol/pc^2\n')
outfile.write('# 6 CO 2-1 luminosity Lsol/pc^2\n')
outfile.write('# 7 CO 3-2 luminosity Lsol/pc^2\n')
outfile.write('# 8 CO 4-3 luminosity Lsol/pc^2\n')
outfile.write('# 9 CO 5-4 luminosity Lsol/pc^2\n')
outfile.write('# 10 CI 1-0 luminosity Lsol/pc^2\n')
outfile.write('# 11 CI 2-1 luminosity Lsol/pc^2\n')
outfile.write('# 12 xCO CO abundance mass weighted\n')
outfile.write('# 13 xCO CO abundance lum weighted\n')
outfile.write('# 14 xCp CII abundance mass weighted\n')
outfile.write('# 15 xCp CII abundance lum weighted\n')
outfile.write('# 16 xC [C] abundance mass weighted \n')
outfile.write('# 17 xC [C] abundance lum weighted\n')
outfile.write('# 18 Tg Kelvin mass weighted\n')
outfile.write('# 19 Tg Kelvin lum weighted\n')
outfile.write('# 20 Td Kelvin mass weighted\n')
outfile.write('# 21 Td Kelvin lum weighted\n')
outfile.write('# 22 Sub_mm luminosity erg/s \n')
outfile.write('# 23 SigHI HI surface density Msun/pc^2 \n')
outfile.close()


#For loops to break things up in chunks
for i in range(len(Z)):
    for j in range(len(Redshift)):
        print 'Working on with step', i, 'of', len(Z)
        print "and step", j, "of", len(Redshift), "within this step"
        #create input grid
        INPUT = np.array(list(itertools.product(Sigma_H2,P,[Z[i]],[Redshift[j]])),dtype = np.float64)
        out = set_up_interpolation(INPUT)
        
        outfile = open(outname,'aw')
        np.savetxt(outfile, np.column_stack((INPUT,np.array(out).T)), fmt = '%1.3e')
        outfile.close()

        print 'Worked for total of', (time.time() - time0)/60, 'minutes'


print 'Worked for', (time.time() - time0)/60, 'minutes'
