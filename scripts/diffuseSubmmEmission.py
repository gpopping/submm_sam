'''radial abundances - plots the abundances of clouds as a function of
their radius (or mass enclosed)

'''
#Hand import DESPOTIC
import sys
sys.path.append('/Users/popping/work/programs/DESPOTIC-2.1')

# Import the despotic library and various standard python libraries
from despotic import cloud,zonedcloud
from despotic.chemistry import NL99_GC
import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u
from astropy import constants as constants
from multiprocessing import Pool
import itertools
import sys, os



########################################################################
# User-settable options
########################################################################

DMR = 1.0
redshift = 0.0
PoolNumber = 4 #number of processors

########################################################################
# Program code
########################################################################
def submm_luminosity(INPUT):
    print INPUT
    Metallicity,RadField = INPUT[0], INPUT[1]


    gmc = cloud()
    #================================================================   
    #The important parameters for the diffuse medium
    gmc.nH = 10. #THE DENSITY
    gmc.colDen = 1.e20 #THE COLUMN DENSITY

    
    #assign other properties of clouds
    gmc.Td = 10.
    gmc.Tg = 10.
    gmc.rad.TradDust = 10.
    gmc.ionRate = 1.e-17*RadField
    gmc.rad.ionRate = 1.e-17*RadField
    gmc.chi = RadField 
    gmc.rad.chi = RadField

    
    #================================================================
    
        
    gmc.sigmaD10   = 2.0e-26  * Metallicity * DMR       # Cross section to 10K thermal radiation, cm^2 H^-1
    gmc.sigmaDPE   = 1.0e-21 * Metallicity * DMR       # Cross section to 8-13.6 eV photons, cm^2 H^-1
    gmc.sigmaDISRF = 3.0e-22  * Metallicity  * DMR      # Cross section to ISRF photons, cm^2 H^-1
    gmc.Zd      = 1.0  * Metallicity * DMR        # Dust abundance relative to solar
    gmc.alphaGD	   = 3.2e-34 * Metallicity * DMR	      # Dust-gas coupling coefficient, erg cm^3 K^-3/2

    gmc.dust.sigmaD10   = 2.0e-26  * Metallicity * DMR       # Cross section to 10K thermal radiation, cm^2 H^-1
    gmc.dust.sigmaDPE   = 1.0e-21 * Metallicity * DMR       # Cross section to 8-13.6 eV photons, cm^2 H^-1
    gmc.dust.sigmaDISRF = 3.0e-22  * Metallicity  * DMR      # Cross section to ISRF photons, cm^2 H^-1
    gmc.dust.Zd      = 1.0  * Metallicity * DMR        # Dust abundance relative to solar
    gmc.dust.alphaGD	   = 3.2e-34 * Metallicity * DMR	      # Dust-gas coupling coefficient, erg cm^3 K^-3/2

    gmc.addEmitter('c+',1.e-100*Metallicity)
    gmc.addEmitter('c',2.e-4*Metallicity)
    gmc.addEmitter('o', 4.e-4*Metallicity)
    gmc.addEmitter('co',1.e-100*Metallicity) 
    gmc.comp.xH2 = 0.0
    gmc.comp.xHe = 0.1
    gmc.comp.H2OPR = 0.25

    #you'll probably want to play with this to make it dynamically
    #depend on the redshift of the slice
    gmc.TCMB = 2.73 * (1. + redshift)
    gmc.rad.TCMB = 2.73 * (1. + redshift)

   
    #================================================================

    #actually run the chemical equilibrium model.  this evolves
    #the temperature calcualtion in iteration with the chemistry
    #which is slower, but the most right thing to do as it
    #simultaneously solves for radiative transfer, chemistry and
    #temperature all at once.
    gmc.comp.computeDerived(gmc.nH)
    gmc.setVirial()
    try:
        gmc.setChemEq(network=NL99_GC, evolveTemp = 'iterateDust', verbose=False, info = {'xC': 2.0e-4*Metallicity,'xO':4.e-4*Metallicity}, tol = 1.e-4)
    except:
        return 0.0, 0.0, 0.0, 0.0, 0.0,0.0,0.0,0.0

    gmclines = gmc.lineLum('co', noClump = True)
    CO10 = gmclines[0]['lumPerH']
    CO21 = gmclines[1]['lumPerH']
    CO32 = gmclines[2]['lumPerH']
    CO43 = gmclines[3]['lumPerH']
    CO54 = gmclines[4]['lumPerH']

    gmclines = gmc.lineLum('c+', noClump = True)
    CII = gmclines[0]['lumPerH']

    gmclines = gmc.lineLum('c', noClump = True)
    CI10 = gmclines[0]['lumPerH']
    CI21 = gmclines[1]['lumPerH']
    return CII/3.826e33,CO10/3.826e33, CO21/3.826e33, CO32/3.826e33, CO43/3.826e33, CO54/3.826e33, CI10/3.826e33, CI21/3.826e33

########################################################################
# Program code
########################################################################
Z = 10**np.linspace(-3,0.5,12)
RadField = 10**np.linspace(-3.,4.0,12)
INPUT = np.array(list(itertools.product(Z,RadField)),dtype = np.float64)


pool = Pool(processes = PoolNumber)
out = np.array(pool.map(submm_luminosity,INPUT))
pool.close()


print 'Write output file...'
outname = 'tables/diffuseEmission_HI.dat'
os.system('rm '+ outname)
outfile = open(outname,'aw')
outfile.write('# 0 Metallicity\n')
outfile.write('# 1 UV Radiation Field (G0)\n')
outfile.write('# 2 CII luminosity Lsol/H\n')
outfile.write('# 3 CO 1-0 luminosity Lsol/H\n')
outfile.write('# 4 CO 2-1 luminosity Lsol/H\n')
outfile.write('# 5 CO 3-2 luminosity Lsol/H\n')
outfile.write('# 6 CO 4-3 luminosity Lsol/H\n')
outfile.write('# 7 CO 5-4 luminosity Lsol/H\n')
outfile.write('# 8 CI 1-0 luminosity Lsol/H\n')
outfile.write('# 9 CI 2-1 luminosity Lsol/H\n')
np.savetxt(outfile, np.column_stack((INPUT,out)), fmt = '%1.3e')
outfile.close()


