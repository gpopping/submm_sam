import sys
sys.path.insert(0, "scripts/")
import assign_luminosities_to_SAM
import time as time
time0 = time.time()

"""
This script starts the process of assigning sub-mm emission line luminosities to SAM generated galaxies (i.e., the multiphase SAM as found on bitbucket). Unless you want to change settings for how luminosities are calculated, there is no need to touch any of the other files.
"""

InDirectory = '/Users/gpopping/work/radtran/C+/data/CII/z0.0/'  #Directory in which output files of the SAM are stored (i.e., galprop.dat etc.)

OutDirectory= '/Users/gpopping/work/radtran/C+/data/CII/CII_lums/Fiducial/'  #Directory wher sub-mm line luminosities should be sotred

OutName = 'z0.0[CII]_lum.dat' #Name of the file in which sub-mm line luminosities will be stored

printLineOnly = False #Only line-luminosities are stored. If turned off, additionaly abundances and temperatures will be stored

ModelType = 'EPS' #The structure of the SAM. Nbody or EPS

EPSredshift = 0.0 #This should be the output redshift of the ESP merger tree

#Call the program that calculates the line luminosity of galaxies
assign_luminosities_to_SAM.run_submm_line_calculator(InDirectory, OutDirectory, OutName, printLineOnly, ModelType = ModelType, EPSredshift = EPSredshift)



print 'Finished in', (time.time() - time0)/60, 'minutes'
