README for the submm extension of the Santa Cruz SAM (Popping, Narayanan et al. 2019).

The purpose is this code is to calculate the sub-mm line emission of galaxies generated with the Santa Cruz SAM.

This README consists of two parts. The first is relevant for users that only want to calculate the sub-mm properties of galaxies from the SAM output. The second part is relevant for users that want to create the lookup tables used to calculate the sub-mm properties of SAM galaxies.

PART 1: The sub-mm emission is calculated by running the script 'get_luminosities.py'. This script calculates the sub-mm properties of galaxies by interpolating through lookup table. The first lookup table is to get the sub-mm properties of the molecular clouds. The second takes the diffuse emission of mainly [CII] into account.

To run this code make sure that:

1) The input and output directories for the SAM galaxies are defined properly (in get_luminosities.py)
2) The interpolator scripts are reading the correct tables (should be stored in the tables directory and be correct by default)

Once you have checked this just run get_luminosities.py and this will automatically create a new file with the same format as the other SAM files in your preferred directory. To run the script you type


>python get_luminosities.py

Alternatively, this script could be started in a slurm environment to properly allocate cores


Part 2:
If you want to create new lookup tables you have to do this with the files in the script directory. 

You will need:
-	DESPOTIC: This comes automatically installed with the distribution. Note that DESPOTIC is not automatically updated
- 	mpi4py. Type 'pip install mpi4py' in your command line
Please contact me (gpopping@gmail.com) if you plan to do this.

---------------------

File description:
createCloudTaskFile.py: File that reads what clouds to calculate from the  cloud parameter file and creates
the disbatch.py Taskfile to run the individual clouds. It will check which of
these clouds are already done (and stored in temp) and once all clouds are
calculated iit will create the final lookup table.

get_luminosities.py: THIS IS THE SCRIPT YOU RUN TO START CALCULATING THE SUB-MM EMISSION LINE LUMINOSITIES OF SAM GALAXIES. In the standard setting there is no need to touch any of the other files

slurmAssignSAMLuminosities: example slurm script to start get_luminosities in batch

slurmCreateCloudTable: example slurm script to start CreateCloudTable.py (on 4 nodes)

slurmCreateCloudTable: example slurm script to start CreateSlabTable.py (on 1 node)

createCloudTaskFile.py: create the disbatch file based on the cloud parameters provided. run as python createCloudTaskFile.py Parameterfile

scripts directory: inluding all the scripts to create lookup tables, interpolate through tables, and calculate the sub-mm line emission of SAM galaxies:
	
	assign_luminosities_to_SAM.py: the script that calculates the sub-mm emission line luminosities of individual galaxies

	CreateSlabTable.py : script to calculate the sub-mm properties of slabs of gas based on individual molecular clouds within slabs/areas of a SAM galaxy

	diffuseSubmmEmission.py: script to calculate the sub-mm emission of diffuse gas

	interpolate_clouds.py: interpolator for the sub-mm properties of individual molecular clouds

	interpolate_diffuse.py: interpolator for the diffuse gas

	interpolate_slabs.py: interpolator to get sub-mm properties for slabs of gas

	OBSOLETE: CreateCloudTable.py: Script to start multi-node computation of sub-mm properties of clouds

	despotic_cloud.py : hart of the code. Script that calculates the sub-mm properties of individual clouds

	the 'despotic' directory: this contains despotic

	LAMBDA directory: directory containing collisional rates for different molecules/atoms
	

the 'parameter files' directory: this contains the parameter files for the creation of a clouds and slabs of gas

tables directory: directory containing all the output tables used for interpolation


